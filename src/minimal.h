#include "audioeffectx.h"

class Minimal : public AudioEffectX{
    public:
        Minimal(audioMasterCallback audioMaster);
        ~Minimal();

        void processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames);
};
