#include "tremolo.h"

#include <math.h>
#include <string>

AudioEffect *createEffectInstance(audioMasterCallback audioMaster) {
    return new Tremolo(audioMaster);
}

Tremolo::Tremolo(audioMasterCallback audioMaster)  : AudioEffectX(audioMaster, 1, 1) {
    freq = 0;
    phase = 0;
    setNumInputs(2);
    setNumOutputs(2);
    canProcessReplacing();
}

Tremolo::~Tremolo() {}

void Tremolo::setParameter(VstInt32 index, float value) {
    freq = exp(10 * value) - 1;
}

float Tremolo::getParameter(VstInt32 index) {
    return log(1 + freq) / 10;
}

void Tremolo::getParameterName(VstInt32 index, char *name) {
    strcpy(name, "Frequency");
}

void Tremolo::getParameterDisplay(VstInt32 index, char *text) {
    strcpy(text, std::to_string(freq).c_str());
}

void Tremolo::getParameterLabel(VstInt32 index, char *label) {
    strcpy(label, "Hz");
}

void Tremolo::processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames) {
    float *in1 = inputs[0];
    float *in2 = inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    float phase_inc = (2 * M_PI * freq) / getSampleRate();

    while(--sampleFrames >= 0) {
        *out1++ = *in1++ * (1 + cos(phase)) / 2;
        *out2++ = *in2++ * (1 + cos(phase)) / 2;
        phase += phase_inc;
        if (phase >= 2 * M_PI)
            phase -= 2 * M_PI;
    }
}
