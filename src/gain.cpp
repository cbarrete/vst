#include "gain.h"

#include <math.h>
#include <string>

AudioEffect *createEffectInstance(audioMasterCallback audioMaster) {
    return new Gain(audioMaster);
}

Gain::Gain(audioMasterCallback audioMaster)  : AudioEffectX(audioMaster, 1, 1) {
    gain = 1;
    setNumInputs(2);
    setNumOutputs(2);
    canProcessReplacing();
}

Gain::~Gain() {}

void Gain::setParameter(VstInt32 index, float value) {
    gain = 2 * value;
}

float Gain::getParameter(VstInt32 index) {
    return gain / 2;
}

void Gain::getParameterName(VstInt32 index, char *name) {
    strcpy(name, "Gain");
}

void Gain::getParameterDisplay(VstInt32 index, char *text) {
    strcpy(text, std::to_string(20*log10(gain)).c_str());
}

void Gain::getParameterLabel(VstInt32 index, char *label) {
    strcpy(label, "dB");
}

void Gain::processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames) {
    float *in1 = inputs[0];
    float *in2 = inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while(--sampleFrames >= 0) {
        *out1++ = *in1++ * gain;
        *out2++ = *in2++ * gain;
    }
}
