#include "audioeffectx.h"

class Phase : public AudioEffectX{
    public:
        Phase(audioMasterCallback audioMaster);
        ~Phase();

        void processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames);

        void setProgramName(char *name)   { strcpy(programName, name); };
        void getProgramName(char *name)   { strcpy(name, programName); };

        bool getEffectName(char *text)    { strcpy(text, "Phase Inverter");    return true; };
        bool getVendorString(char *text)  { strcpy(text, "C. Barreteau");      return true; };
        bool getProductString(char *text) { strcpy(text, "CB Phase Inverter"); return true; };
        VstInt32 getVendorVersion()       { return 1; }

    protected:
        char programName[32];
};
