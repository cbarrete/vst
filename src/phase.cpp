#include "phase.h"

#include <math.h>
#include <string>

AudioEffect *createEffectInstance(audioMasterCallback audioMaster) {
    return new Phase(audioMaster);
}

Phase::Phase(audioMasterCallback audioMaster)  : AudioEffectX(audioMaster, 1, 0) {
    setNumInputs(2);
    setNumOutputs(2);
    canProcessReplacing();
}

Phase::~Phase() {}

void Phase::processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames) {
    float *in1 = inputs[0];
    float *in2 = inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    while(--sampleFrames >= 0) {
        *out1++ = -*in1++;
        *out2++ = -*in2++;
    }
}
