#include "fold.h"

#include <math.h>
#include <string>

AudioEffect *createEffectInstance(audioMasterCallback audioMaster) {
    return new Fold(audioMaster);
}

Fold::Fold(audioMasterCallback audioMaster)  : AudioEffectX(audioMaster, 1, 2) {
    gain = 1;
    setNumInputs(2);
    setNumOutputs(2);
    canProcessReplacing();
}

Fold::~Fold() {}

void Fold::setParameter(VstInt32 index, float value) {
    switch (index) {
        case 0:
            gain = (10 * value) + 1;
            break;
        case 1:
            func = (value < 0.5) ? SINE : COSINE;
            break;
    }
}

float Fold::getParameter(VstInt32 index) {
    switch (index) {
        case 0:
            return (gain -1) / 10;
        case 1:
            return func;
        default:
            return 0;
    }
}

void Fold::getParameterName(VstInt32 index, char *name) {
    switch (index) {
        case 0:
            strcpy(name, "Gain");
            break;
        case 1:
            strcpy(name, "Function");
            break;
        default:
            strcpy(name, "error");
    }
}

void Fold::getParameterDisplay(VstInt32 index, char *text) {
    switch (index) {
        case 0:
            strcpy(text, std::to_string(20*log10(gain)).c_str());
            break;
        case 1:
            switch (func) {
                case SINE:
                    strcpy(text, "Sine");
                    break;
                case COSINE:
                    strcpy(text, "Cosine");
                    break;
                default:
                    strcpy(text, "error");
                    break;
            }
            break;
        default:
            strcpy(text, "error");
    }
}

void Fold::getParameterLabel(VstInt32 index, char *label) {
    switch (index) {
        case 0:
            strcpy(label, "dB");
            break;
        case 1:
            strcpy(label, "");
            break;
        default:
            strcpy(label, "error");
    }
}

void Fold::processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames) {
    float *in1 = inputs[0];
    float *in2 = inputs[1];
    float *out1 = outputs[0];
    float *out2 = outputs[1];

    if (func == SINE)
        while(--sampleFrames >= 0) {
            *out1++ = sin(*in1++ * gain);
            *out2++ = sin(*in2++ * gain);
        }
    else
        while(--sampleFrames >= 0) {
            *out1++ = cos(*in1++ * gain);
            *out2++ = cos(*in2++ * gain);
        }
}
