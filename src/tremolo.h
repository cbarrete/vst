#include "audioeffectx.h"

class Tremolo : public AudioEffectX{
    public:
        Tremolo(audioMasterCallback audioMaster);
        ~Tremolo();

        void processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames);

        void  setParameter(VstInt32 index, float value);
        float getParameter(VstInt32 index);
        void  getParameterLabel(VstInt32 index, char *label);
        void  getParameterDisplay(VstInt32 index, char *text);
        void  getParameterName(VstInt32 index, char *text);

        void setProgramName(char *name)   { strcpy(programName, name); };
        void getProgramName(char *name)   { strcpy(name, programName); };

        bool getEffectName(char *text)    { strcpy(text, "Tremolo");      return true; };
        bool getVendorString(char *text)  { strcpy(text, "C. Barreteau"); return true; };
        bool getProductString(char *text) { strcpy(text, "CB Tremolo");   return true; };
        VstInt32 getVendorVersion()       { return 1; }

    protected:
        float phase;
        float freq;

        char programName[32];
};
