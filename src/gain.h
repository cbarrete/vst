#include "audioeffectx.h"

class Gain : public AudioEffectX{
    public:
        Gain(audioMasterCallback audioMaster);
        ~Gain();

        void processReplacing(float **inputs, float **outputs, VstInt32 sampleFrames);

        // parameters
        void  setParameter(VstInt32 index, float value);
        float getParameter(VstInt32 index);
        void  getParameterLabel(VstInt32 index, char *label);  // unit
        void  getParameterDisplay(VstInt32 index, char *text); // value
        void  getParameterName(VstInt32 index, char *text);    // name

    protected:
        float gain;
};
