CC = g++
PLUGINS = minimal gain phase fold tremolo
SRC = ./src/
SDK = ./sdk/

CFLAGS = -Wall -O2 -c -mmmx -fPIC -fno-exceptions -I$(SDK)
LFLAGS = -Wall -s -shared -lpthread

VST = linuxmain.o vstplugmain.o audioeffect.o audioeffectx.o
OBJPLUG = $(PLUGINS:=.so)


all: $(OBJPLUG)
	@echo compilation finished

clean:
	rm -f *.o *.so


%.o: $(SRC)%.cpp
	$(CC) $(CFLAGS) $<

%.so: %.o $(VST)
	$(CC) $(LFLAGS) $*.o $(VST) -o $*.so

linuxmain.o: linuxmain.cpp
	$(CC) $(CFLAGS) linuxmain.cpp

vstplugmain.o: $(SDK)vstplugmain.cpp
	$(CC) $(CFLAGS) $(SDK)vstplugmain.cpp

audioeffect.o: $(SDK)audioeffect.cpp
	$(CC) $(CFLAGS) $(SDK)audioeffect.cpp

audioeffectx.o: $(SDK)audioeffectx.cpp
	$(CC) $(CFLAGS) $(SDK)audioeffectx.cpp
