# Writing VST2 plugins

## Warning

The examples presented here will only compile on Linux-based systems. Windows
plugins will be covered in the future.

## Scope

The goal of this project is to introduce the VST2 format and show how to use its
SDK to write minimal example plugins upon which one could build real audio
projects.

This project does *not* intend to showcase modern and/or idiomatic C++ but
rather portable and accessible code.

## Why VST2?

The VST format authored by Steinberg is used to define audio plugins. It is
supported by most digital audio workstations on all major platforms. It allows
to abstract the system that it is running over and is attractive to musicians,
who can seamlessly integrate VST plugins to their workflow. Since the host can
offer a default GUI interface if the plugin does not define one, the developer
can focus solely on audio processing while benefiting from a well integrated
look and feel.

The VST2 SDK is lightweight and easy to use, making it ideal to learn for
beginners.

The newer VST3 SDK has some advantages over it. It is available under the free
GPLv3 license, offers more advanced features and a better separation of concerns
between the user interface and the audio processor. However, it is also a lot
heavier and requires many dependencies, including `cmake` as a build system and
platform specific graphics libraries.

## Project layout

This project contains several plugins, which are all located under the `src`
directory.  These plugins grow in complexity as new features offered by the SDK
are introduced. All these plugins can be compiled from the Makefile located in
the root directory.

## Requirements

This project assumes that the following requirements are fullfilled:
- knowledge of C++ basics (classes, inheritance, construction/destruction)
- C++ compiler installed
- VST2 SDK in the correct location
- `make` build system installed (optional but highly recommended)

### Knowledge of C++ basics

The VST2 SDK requires implementing a virtual class. Consequently, prior
knowledge of C++ basics, such as classes, inheritance, construction and
destruction are required before learning how to write VST plugins.

### C++ compiler

Any C++ compiler should be able to compile the examples presented in this
project. Obviously, compilers supporting newer versions of the standard will
enable the use of more modern C++ features.

The Makefile included in this project uses `gcc` by default.

### SDK

Since the VST2 SDK is not avaiable under a free license, it must be downloaded
separately from [Steinberg's
website](https://www.steinberg.net/en/company/developers.html) and extracted in
the `sdk` directory. This location is optional but will ensure compatibility
with the Makefile included in this project.

The resulting file structure should look like this:

```
.
├── linuxmain.cpp
├── Makefile
├── README.md
├── ...
├── sdk
│   ├── aeffeditor.h
│   ├── audioeffect.cpp
│   ├── audioeffect.h
│   ├── audioeffectx.cpp
│   ├── audioeffectx.h
│   ├── pluginterfaces
│   │   ├── base
│   │   │   └── ...
│   │   ├── gui
│   │   │   └── ...
│   │   ├── test
│   │   │   └── ...
│   │   ├── vst
│   │   │   └── ...
│   │   └── vst2.x
│   │       └── ...
│   └── vstplugmain.cpp
└── src
    └── ...
```

## Plugins

### minimal

This example showcases a very minimal plugin. Its only effect is to half the
gain of any of its input.

The constructor extends the AudioEffectX class that is provided by the SDK. It
passes the amount of programs (1) and parameters (0) of the plugin to the
AudioEffectX constructor. It then sets the amount of input and output channels
(2 in both cases since our plugin is stereo to stereo). Finally, it enables
`processReplacing` processing. The default behavior of a plugin is to use the
`process` virtual method. It is used to add signal to the input, which can be
desired for primitive implementations of effects such as delay or reverb.
However, `processReplacing` completely replaces the input, which is often more
desirable and easier to manipulate.

We finally implement the `processReplacing` method, which takes double pointers
for input and output, and a `sampleFrame` integer. The sample values are
accessible by dereferencing the pointers once to get the desired channel and a
second time to get the specific value required.
The user is expected to process the values taken from `inputs` and to store them
in `outputs`, which is automatically allocated. `sampleFrame` counts the amount
of samples available in each channel.

### gain

This example is a simple parametrized gain plugin, that can attenuate the input
signal as much as wanted or boost it up to +6dB.

Taking the minimal plugin as a base, we increment the parameter count passed to
the AudioEffectX constructor and add a protected attribute to the plugin's
class. We declare methods that allow us to get, set and display that parameter
in `gain.h` and implement them in `gain.cpp`.

Since we only have one parameter, we don't need to use the index parameter in
the `getParameter` and `setParameter` methods. The `value` given to
`setParameter` and the one returned `getParameter` corresponds to the position
of the knob in the plugin's UI in a range from 0 to 1. Thus, we multiply/divide
by 2 accordingly to allow our `gain` attribute to go over 1.

`getParameterName` must store the description of the parameter in `name`.

`getParameterDisplay` must store the value of the parameter in `text`. In our
case, we convert the value of `gain` in decibels, which is more
musician-friendly.

`getParameterLabel` must store the unit of the parameter in `name`.

Finally, we alter the processing loop to make it store the inputs multiplied by
the gain in the outputs.

### phase

This example juste flips the phase of the incoming signal by multiplying all
samples by -1.

However, it also implements some functions that are useful for the host and the
user.

`getEffectName` must set it's argument to the name of the effect. This is shown
to the user in the host's plugin picker.

`getVendorString` must set it's argument to the vendor's name. This is shown
to the user in the host's plugin picker.

`getProductString` must set it's argument to the plugin's product string. This
is not necessarily shown to the user and should be used to properly determine
the plugin. It could simply be a concatenation of the vendor's name and the
effect's name.

`getVendorVersion` must return the plugin's version. The return value should be
unique for each released version.

### fold

This example showcases how to handle use multiple parameters. It adds (a lot) of
gain to the input and folds it back between -1 and 1 by passing it through
either the sine or cosine function.

The different parameters are accessed by simply matching against the `index`
argument that is passed to most functions. The index values are determined by
the number of arguments that the plugin declares and start 0.

**Note:** using the cosine function in this plugin introduces DC offset when
there is no input since $`cos(0)=1`$. This behavior has not been modified to
keep the code as concise as possible.
